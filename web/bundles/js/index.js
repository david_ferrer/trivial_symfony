$(function () {
	console.log("index.js Carregat");
	$("#btnLogin, .highlogin").on('click', showLoginForm);
	
  /* If you click anywhere other than 'modal-content' from modal, it close */
  $('.my-modal').on('click', function(e){
    if( $('.my-modal').hasClass('active') && $(e.target).hasClass('my-modal') )
      closeAnyActiveModal();
  });
  
  $("#submit-login-form").on('click', checkAndLogin);
	
});

function showLoginForm() {
	$(".highlogin").hide();
	$("#login").addClass("active");
	$("#username").focus();
}

function closeAnyActiveModal(){
	$(".my-modal.active").addClass("closing");
	setTimeout( retardedClosingEffect, 500 );
}

function retardedClosingEffect(){
	$(".my-modal.active").removeClass("closing");
	$(".my-modal.active").removeClass("active");
}

function checkAndLogin() {
	console.log($("#name").val());
	console.log($("#pass").val());
}