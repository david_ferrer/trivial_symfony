// 		GLOBAL
var cVerd = "#4DAB53";
var cMarro = "#A86C38";
var cRosa = "#FD85B3";
var cGroc = "#EFDB3A";
var cBlau = "#5FBFCD";
var cTronja = "#F17B35";
var cBlanc = "#EBCFC3";
var cFons = "#F6E0D7";
var cFonsTauler = "#317081";
var cBuit = "";

var nColumns = 6;
var nBtnsExtPerPart = 7;
var nBtnsPerColumn = 5;
var nBtnsExt = 42;
var separator = 20;

var canvas, ctxt, fitxaWidth, fitxaHeight, arrayBtnsExteriors = [];

var numDau;
var indexBotoActualJugador;

// 		GLOBAL BOOLS
var isOnForkBtnBool;
var isOnExternalBtnBool;
var isOnColumnBtnBool;
var isOnStartBbtnBool;

var colExacteBtnColumn;
var btnExacteBtnColumn;
var fitxaDelJugadorAmbTorn;
var dauTirat;
var tornActiu;

var seccio1, seccio2, seccio3, seccio4, seccio5, seccio6;

var colorsColumns = [
	[cVerd, cMarro, cRosa, cGroc, cBlau],
	[cMarro, cBlau, cVerd, cRosa, cTronja],
	[cBlau, cTronja, cMarro, cVerd, cGroc],
	[cTronja, cGroc, cBlau, cMarro, cRosa],
	[cGroc, cRosa, cTronja, cBlau, cVerd],
	[cRosa, cVerd, cGroc, cTronja, cMarro]
];

var colorsBtnsExt = [
	[cMarro, cGroc, cBlanc, cTronja, cVerd, cBlanc, cRosa],
	[cBlau, cRosa, cBlanc, cGroc, cMarro, cBlanc, cVerd],
	[cTronja, cVerd, cBlanc, cRosa, cBlau, cBlanc, cMarro],
	[cGroc, cMarro, cBlanc, cVerd, cTronja, cBlanc, cBlau],
	[cRosa, cBlau, cBlanc, cMarro, cGroc, cBlanc, cTronja],
	[cVerd, cTronja, cBlanc, cBlau, cRosa, cBlanc, cGroc],
];

var colorsJugadors = [cMarro, cBlau, cTronja, cGroc, cRosa, cVerd];

// jQuery.ready();
$(function() {
	
	creaBtnsInterns();
	creaBtnsExteriors();
	analisiInicial();
	creaCanvasJugadors();
	
	//			DELETE
	fitxaDelJugadorAmbTorn = "fitxa1";
	//$("#btnCentral").addClass(fitxaDelJugadorAmbTorn);
	dauTirat = false;
		
});

function creaBtnsInterns() {
	for (i=0; i<nColumns; i++){
		var col = document.createElement("DIV");
		col.setAttribute("class", "colClass");
		col.setAttribute("id", "col"+(i+1));

		for (j=0; j<nBtnsPerColumn; j++){
			btn = document.createElement("BUTTON");
			btn.setAttribute("id", "col"+(i+1)+"btn"+(j+1));
			btn.setAttribute("class", "btnClass");
			btn.style.backgroundColor = colorsColumns[i][j];
			col.appendChild(btn);
		}
		$("#c"+ (i+1)).append(col);
		$("#col"+ (i+1)).css({
			"width":"100%",
			"height":separator + "%",
			"position":"absolute"
		});		
	}
}

function creaBtnsExteriors() {
	var auxAngle = 55;
	var auxIdIndex = 1;
	for(i=0; i<nColumns; i++){
		for(j=0; j<nBtnsExtPerPart; j++){ 
			btn = document.createElement("BUTTON");
			btn.setAttribute("id", "extbtn"+(auxIdIndex++));
			btn.setAttribute("class", "btnExtClass");
			if (j%nBtnsExtPerPart == 0){
				btn.setAttribute("class", "btnExtClass fork"+(i+1));
				btn.style.height = "50px";
				btn.style.zIndex = "1";
				btn.style.border = "3px solid " + colorsBtnsExt[i][j];
				btn.style.backgroundColor = cBlanc; 
			} else {
				btn.style.backgroundColor = colorsBtnsExt[i][j];
			}
			btn.style.transform = 
				"rotate(" + (((auxAngle/nBtnsExtPerPart) * j) + (i*60)) + "deg)";		
			$(document.body).prepend(btn);
		}
	}
}

function analisiInicial() {
	fitxaWidth = $(".fitxa").innerWidth();
	fitxaHeight = $(".fitxa").innerHeight();
	$("#btnCentral").on('click', mouFitxa).addClass("clickable");
	
	// juntem en un array tots els botons exteriors
	$btnExteriors = $("button[id^='extbtn']");
	len = $btnExteriors.length;
	for (i=0; i<len; i++) {
		arrayBtnsExteriors[i] = $btnExteriors.get(len -i -1); 
	}
	
	// indiquem als botons el seu clic
	$("button").off('click');
	$("button").on('click', mouFitxa);
	// però al de prova no
	$("#btnIniciaRonda").off('click');
	$("#btnTiraDau").off('click');
	
}

function creaCanvasJugadors() {
	for (i=0; i<6; i++) {
		canvas = document.getElementById("j" + (i+1) );
		ctxt = canvas.getContext("2d");
		
		ctxt.beginPath();
		ctxt.arc(75, 75, 50, 0, 2 * Math.PI);
		ctxt.fillStyle = colorsJugadors[i];
		ctxt.fill();
		
		$(".borderj" + (i+1) ).css("border", "10px solid " + colorsJugadors[i]);
		
		ctxt.beginPath();
		ctxt.font = "20px sans-serif";
		ctxt.textBaseline = "top";
		ctxt.fillText ("Jugador " + (i+1), 175, 70 );
		
		ctxt.beginPath();
		ctxt.font = "45px sans-serif";
		ctxt.textBaseline = "top";
		ctxt.fillStyle = "white";
		ctxt.fillText ("J" + (i+1), 50, 50 );
	}
}

function mouFitxa(eBtn) {
	
	// afegim classe al botó per indicar 
	// de manera senzilla on està la fitxa del jugador
	$("button").removeClass(fitxaDelJugadorAmbTorn);
	console.log("afegim classe de la fitxa amb torn");
	$(this).addClass(fitxaDelJugadorAmbTorn); 
	
	$(".active").css({
		left: (eBtn.pageX - fitxaWidth/2) + "px",
		top: (eBtn.pageY - fitxaHeight/2) + "px"
	});
	
	finalitzaTorn();
}

function finalitzaTorn() {
	dauTirat = true;
	tornActiu = false;
	console.log("Finalitza torn");
} 

function inciaTorn() {
	
	console.log("----------- TORN ------------");
	
	tornActiu = true;
	netejaParametres();	
	netejaButons();
	
	if (dauTirat){
		inhabilitaTotsElsBotons();
		calculaNumDau();
		trobaPosicioJugador();
		mostraPossiblesCamins();
		//mostraBotonsAmbFitxa();
	} else {
		alert("has de tirar el DAU");
	}
}

function netejaParametres() {
	isOnForkBtnBool = false;
	isOnExternalBtnBool = false;
	isOnColumnBtnBool = false;
	isOnStartBbtnBool = false;
	colExacteBtnColumn = "";
	btnExacteBtnColumn = "";
	seccio1 = false;
	seccio2 = false;
	seccio3 = false;
	seccio4 = false;
	seccio5 = false;
	seccio6 = false;
}

function netejaButons() {
	$("button").removeClass("no-clickable");
	$("button").removeClass("clickable");
}

function inhabilitaTotsElsBotons() {
	// el botó que no contingut la fitxa del jugador amb torn serà no-clicable
	$("button").not("#btnIniciaRonda").not("#btnTiraDau").off('click').addClass("no-clickable");
}

function inhabilitaBotoConcret(id) {
	$("#"+id).removeClass("clickable");
	$("#"+id).addClass("no-clickable");	
}

function tiraDau() {
	console.log("tirem dau");
	numDau = $("input").val();
	dauTirat = true;
}

function calculaNumDau() {
	numDau = $("input").val();	
	console.log("DAU: " + numDau);
}

// aquí comprovem si la fitxa del jugador es troba en...
//		· botons exteriors (botó fork o no)
//		· en una de les columnes
//		· boto central incial
// i després, segons on es troba, es cridaran uns mètodes o uns altres
function trobaPosicioJugador() {
	console.log("_trobaPosicioJugador() METHOD");
	$btnPlayer = $("button."+fitxaDelJugadorAmbTorn);
	auxIndex = 0;
	indexBotoActualJugador = null;
	
	console.log("ID del boto a trobar: " + $btnPlayer.attr("id"));
	
	////////////////////////////////////////////////////
	// comprovem si es troba en un dels botons exteriors
	////////////////////////////////////////////////////
	arrayBtnsExteriors.forEach(function (entry) {
		if ($btnPlayer.attr("id") == entry.getAttribute("id")){
			indexBotoActualJugador = auxIndex;
			isOnExternalBtnBool = true;
			
			/////////////////////////////////////////
			// i si es tracta d'un fork exterior o no
			/////////////////////////////////////////
			if ( isOnForkBtnMethod (indexBotoActualJugador) )
				isOnForkBtnBool = true;
		}
		auxIndex++;
	});

	
	///////////////////////////////////////////////
	// comprovem si es troba en una de les columnes
	///////////////////////////////////////////////
	if ( isOnColumnBtnMethod(fitxaDelJugadorAmbTorn) ){
		isOnColumnBtnBool = true;
	}
	
	///////////////////////////////////////////
	// comprovem si es troba en el botó central
	///////////////////////////////////////////
	if ( isOnStartBtnMethod() ){
		isOnStartBbtnBool = true;
	}
			
}

function mostraPossiblesCamins() {
	console.log("_mostraPossiblesCamins() METHOD");
	
	// si està en els botons exteriors però no fork
	if (isOnExternalBtnBool && !isOnForkBtnBool){
		console.log("Btn exterior non-fork: " + indexBotoActualJugador);
		
		// ara volem saber en quina secció es troba exactament per gestionar camins dels forks exteriors
		// 	· seccio1 compren els botons intermitjos entre el fork1 i el fork2
		// 	· seccio2 compren els botons intermitjos entre el fork2 i el fork3
		// 	· etc..
		determinaSeccio();
		iluminaBifurcacionsAmbPossiblesForks();
	}
	// si està en els botons exteriors sent fork 
	else if (isOnForkBtnBool && isOnExternalBtnBool){
		console.log("Btn exterior Fork");
		forkClassAttr = arrayBtnsExteriors[indexBotoActualJugador].getAttribute("class");
		iluminaBifurcacionsForks(forkClassAttr, numDau);
	}
	// si està en alguna columna.. 
	else if (isOnColumnBtnBool){
		console.log("EN COLUMNA col["+colExacteBtnColumn + "] btn["+btnExacteBtnColumn + "]");
		iluminaBifuracacionsColumna();
	}
	// si està en el botó incial
	else if (isOnStartBbtnBool){
		console.log("ET TROBES AL BOTO INCIAL!");
		iluminaBifurcacionsInicial(numDau);
	} else {
		console.log("Error de ronda, no entra en cap estat¿?¿?");
	}
	
}



function determinaSeccio() {
	ind = indexBotoActualJugador;
	if (ind > 0 && ind < 7)
		seccio1 = true;
	else if (ind > 7 && ind < 14)
		seccio2 = true;
	else if (ind > 14 && ind < 21)
		seccio3 = true;
	else if (ind > 21 && ind < 28)
		seccio4 = true;
	else if (ind > 28 && ind < 35)
		seccio5 = true;
	else if (ind > 35 && ind < 42)
		seccio6 = true;
		
	if (seccio1) console.log("-/ S1");
	if (seccio2) console.log("-/ S2");
	if (seccio3) console.log("-/ S3");
	if (seccio4) console.log("-/ S4");
	if (seccio5) console.log("-/ S5");
	if (seccio6) console.log("-/ S6");
}

function iluminaBifurcacionsAmbPossiblesForks() {
	
	var auxMovimentsRestants = numDau;
	var len = arrayBtnsExteriors.length;
	var forkTrobatDavant = false;
	var forkTrobatDarrere = false;
	var indexEndavant = indexBotoActualJugador;
	var indexEnrrere = indexBotoActualJugador;
		
	for(i=0; i<numDau; i++){
		auxMovimentsRestants--;
		indexEndavant++;
		indexEnrrere--;
		if (indexEndavant > 41)
			indexEndavant = 0;
		if (indexEnrrere < 0)
			indexEnrrere = 0;
		
		console.log("[" + indexBotoActualJugador + "] index Actual");
		console.log("[" + indexEndavant + "] index següent");
		console.log("[" + indexEnrrere + "] index enrrere");
		console.log("[" + auxMovimentsRestants + "] moviments restants");
		
		// Cap endavant
		if ( !isOnForkBtnBool && !forkTrobatDavant ){	
			forkClassAttr = getForkClass(arrayBtnsExteriors[indexEndavant]);
			if (forkClassAttr != null){
				console.log("FORK TROBAT ENDAVANT");
				forkTrobatDavant = true;
				iluminaBifurcacionsForks(forkClassAttr, auxMovimentsRestants);
			}
		}
		
		
		// Cap enrrere
		if ( !isOnForkBtnBool && !forkTrobatDarrere ){			
			forkClassAttr = getForkClass(arrayBtnsExteriors[indexEnrrere]);
			if (forkClassAttr != null){
				console.log("FORK TROBAT ENRRERE");
				forkTrobatDarrere = true;
				iluminaBifurcacionsForks(forkClassAttr, auxMovimentsRestants);
			}
		}
	}
	
	// si no ha trobat cap fork cap endavant..
	if (!forkTrobatDavant){
		console.log("no s'ha trobat fork >>..");
		nextIndex = parseInt(indexBotoActualJugador, 10) + parseInt(numDau, 10);
		console.log("[" + nextIndex + "] iluminem botó suelto >>");
		fesElBotoClicable(arrayBtnsExteriors[nextIndex]);
	}
	if (!forkTrobatDarrere){
		console.log("no s'ha trobat fork <<..");
		prevIndex = parseInt(indexBotoActualJugador, 10) - parseInt(numDau, 10);
		console.log("[" + prevIndex + "] iluminem botó suelto <<");
		fesElBotoClicable(arrayBtnsExteriors[prevIndex]);
	}
}


function mostraBotonsAmbFitxa() {
	$("."+fitxaDelJugadorAmbTorn).each(function (btnEvent) {
		console.log("BTN FITXA! -> " + btnEvent.id);
	});
}

function isOnForkBtnMethod(indexPerBuscar) {
	
	if (
		indexPerBuscar == 0 ||
		indexPerBuscar == 7 ||
		indexPerBuscar == 14 ||
		indexPerBuscar == 21 ||
		indexPerBuscar == 28 ||
		indexPerBuscar == 35
	)
		return true;
	
	return false;
}

function isOnColumnBtnMethod() {
	idAttr = $("."+fitxaDelJugadorAmbTorn).attr("id");
	
	//console.log("id del boto en columna..: " + idAttr);
	if (idAttr != "undefined" && idAttr != null)
		if (idAttr.includes("col")){
			for(i=0; i<nColumns; i++) {
				for(j=0; j<nColumns; j++) {
					if ( idAttr.includes("col"+(i+1)+"btn"+(j+1)) ){
						colExacteBtnColumn = (i+1);	// guardem la columna
						btnExacteBtnColumn = (j+1);	// guardem el boto
						return true;	
					}
				}
			}
		}
	
	return false;
	
}

function isOnStartBtnMethod() {
	if ($("#btnCentral").hasClass(fitxaDelJugadorAmbTorn)){
		return true;
	}
	
	return false;
}

/* DELETE
function indicaCamins() {
	
	for(i=0; i<arrayBtnsExteriors.length; i++){
		if (indexBotoActualJugador == arrayBtnsExteriors.length)
			;
	}
	
}
*/

/* DELETE
function movimentCapEndavant() {	
}
*/



// aquesta funció afecta als 
//		- objectesDOM (btnObj)
//		- objectes de jQuery ($btnObj)
//		- si els dos anteriors són nuls llavors es trcta d'un boto en columna
//		  per tant tindrem la posició exacte en les variables:
//		  colExacteBtnColumn i btnExacteBtnColumn;
// d'aquesta manera em facilito el passar els botons de les tres formes 
function fesElBotoClicable(btnObj, $btnObj, id) {
	// obtenim si té una classe fork		
	auxForkClass = getForkClass(btnObj, $btnObj, id);
	forkBtn = false;
	if (auxForkClass != null){
		console.log("["+auxForkClass+"] FORK BTN!");
		forkBtn = true;
	}
	
	// treiem onClick i efecte CLICK en el mouse
	if (btnObj != null){
		btnObj.setAttribute("class", "");
		
		if (forkBtn)
			btnObj.setAttribute("class", "btnExtClass clickable "+auxForkClass);
		else
			btnObj.setAttribute("class", "btnExtClass clickable");
			
		btnObj.onclick = mouFitxa;
	} else if ($btnObj != null){	
		$btnObj.attr("class", "");
		
		if (forkBtn)
			$btnObj.addClass("btnClass clickable "+auxForkClass);
		else
			$btnObj.addClass("btnClass clickable");
			
		$btnObj.on('click', mouFitxa);
	} else if (id != null){
		$("#"+id).attr("class", "");
		
		if (forkBtn)
			$("#"+id).addClass("btnClass clickable "+auxForkClass);
		else
			$("#"+id).addClass("btnClass clickable");
			
		$("#"+id).on('click', mouFitxa);
	}
}

function getForkClass(btnObj, $btnObj, id) {
	
	classAttr = null;
	if (btnObj != null)
		classAttr = btnObj.getAttribute("class");
	else if ($btnObj != null)
		classAttr = $btnObj.attr("class");
	else if (id != null){
		classAttr = document.getElementById(id).getAttribute("class");
	}
	if (classAttr != null){
		if (classAttr.includes("fork")){
			if (classAttr.includes("fork1")){
				return "fork1";
			} else if (classAttr.includes("fork2")) {
				return "fork2";
			} else if (classAttr.includes("fork3")) {
				return "fork3";
			} else if (classAttr.includes("fork4")) {
				return "fork4";
			} else if (classAttr.includes("fork5")) {
				return "fork5";
			} else if (classAttr.includes("fork6")) {
				return "fork6";
			}
		}
	}
	
	return null;
}


// 		ILUMINA BIFURCACIONS

function iluminaBifurcacionsForks(forkClassAttr, numDauRestant){

	if (forkClassAttr.includes("fork")){
		if (forkClassAttr.includes("fork1")){
			console.log("fork1");
			iluminaBifurcacionsFork1(numDauRestant);
		} else if (forkClassAttr.includes("fork2")) {
			console.log("fork2");
			iluminaBifurcacionsFork2(numDauRestant);
		} else if (forkClassAttr.includes("fork3")) {
			console.log("fork3");
			iluminaBifurcacionsFork3(numDauRestant);
		} else if (forkClassAttr.includes("fork4")) {
			console.log("fork4");
			iluminaBifurcacionsFork4(numDauRestant);
		} else if (forkClassAttr.includes("fork5")) {
			console.log("fork5");
			iluminaBifurcacionsFork5(numDauRestant);
		} else if (forkClassAttr.includes("fork6")) {
			console.log("fork6");
			iluminaBifurcacionsFork6(numDauRestant);
		}
	}	
	
}

function iluminaBifuracacionsColumna() {
	var btnAux;
	
	// DELETE
	//console.log("Dau: " + numDau);
	//console.log("Col: " + colExacteBtnColumn);
	//console.log("Btn: " + btnExacteBtnColumn);
	
	if (btnExacteBtnColumn == 1){
		if (numDau == 1){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 0);
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+2);
		} else if (numDau == 2){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 1);
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+3);
		} else if (numDau == 3){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 2);
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+4);
		} else if (numDau == 4){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 3);
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+5);
		} else if (numDau == 5){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 4);
			iluminaBifurcacionsInicial(0);
		} else if (numDau == 6){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 5);
			iluminaBifurcacionsInicial(1);
		}
	} else if (btnExacteBtnColumn == 2){
		if (numDau == 1){
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+1);
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+3);
		} else if (numDau == 2){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 0);
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+4);
		} else if (numDau == 3){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 1);
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+5);
		} else if (numDau == 4){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 2);
			iluminaBifurcacionsInicial(0);
		} else if (numDau == 5){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 3);
			iluminaBifurcacionsInicial(1);
		} else if (numDau == 6){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 4);
			iluminaBifurcacionsInicial(2);			
		}
	} else if (btnExacteBtnColumn == 3){
		if (numDau == 1){
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+2);
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+4);
		} else if (numDau == 2){
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+1);
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+5);
		} else if (numDau == 3){
			iluminaBifurcacionsInicial(0);
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 0);
		} else if (numDau == 4){
			iluminaBifurcacionsInicial(1);
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 1);
		} else if (numDau == 5){
			iluminaBifurcacionsInicial(2);
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 2);
		} else if (numDau == 6){
			iluminaBifurcacionsInicial(3);
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 3);
		}
	} else if (btnExacteBtnColumn == 4){
		if (numDau == 1){
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+3);
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+5);
		} else if (numDau == 2){
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+2);
			iluminaBifurcacionsInicial(0);
		} else if (numDau == 3){
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+1);
			iluminaBifurcacionsInicial(1);
		} else if (numDau == 4){
			iluminaBifurcacionsInicial(2);
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 0);
		} else if (numDau == 5){
			iluminaBifurcacionsInicial(3);
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 1);
		} else if (numDau == 6){
			iluminaBifurcacionsInicial(4);
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 2);
		}
	} else if (btnExacteBtnColumn == 5){
		if (numDau == 1){
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+4);
			iluminaBifurcacionsInicial(0);
		} else if (numDau == 2){
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+3);
			iluminaBifurcacionsInicial(1);			
		} else if (numDau == 3){
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+2);
			iluminaBifurcacionsInicial(2);
		} else if (numDau == 4){
			fesElBotoClicable(null, null, "col"+colExacteBtnColumn+"btn"+1);
			iluminaBifurcacionsInicial(3);
		} else if (numDau == 5){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 0);
			iluminaBifurcacionsInicial(4);
		} else if (numDau == 6){
			iluminaBifurcacionsSegonsColumna(colExacteBtnColumn, 1);
			iluminaBifurcacionsInicial(5);
		}
	}
}

function iluminaBifurcacionsSegonsColumna(col, dau){
	if (dau == 0){
		if (col == 1){
			auxForkBtn = document.getElementsByClassName("fork3")[0];			
		} else if (col == 2){
			auxForkBtn = document.getElementsByClassName("fork4")[0];
		}else if (col == 3){
			auxForkBtn = document.getElementsByClassName("fork5")[0];
		}else if (col == 4){
			auxForkBtn = document.getElementsByClassName("fork6")[0];
		}else if (col == 5){
			auxForkBtn = document.getElementsByClassName("fork1")[0];
		}else if (col == 6){
			auxForkBtn = document.getElementsByClassName("fork2")[0];
		}
					
		fesElBotoClicable(auxForkBtn, null, null);
	}		
	else if (col == 1)
		iluminaBifurcacionsFork3(dau);
	else if (col == 2)
		iluminaBifurcacionsFork4(dau);
	else if (col == 3)
		iluminaBifurcacionsFork5(dau);
	else if (col == 4)
		iluminaBifurcacionsFork6(dau);
	else if (col == 5)
		iluminaBifurcacionsFork1(dau);
	else if (col == 6)
		iluminaBifurcacionsFork2(dau);
}

function iluminaBifurcacionsInicial(numDauRestant) {
	if (numDauRestant == 0){
		$("#btnCentral")
			.attr("class", "")
			.addClass("btnCentral clickable")
			.off('click')
			.on('click', mouFitxa);		
	} else if (numDauRestant == 1){
		fesElBotoClicable(null, $("#col1btn5"), null);
		fesElBotoClicable(null, $("#col2btn5"), null );
		fesElBotoClicable(null, $("#col3btn5"), null );
		fesElBotoClicable(null, $("#col4btn5"), null );
		fesElBotoClicable(null, $("#col5btn5"), null );
		fesElBotoClicable(null, $("#col6btn5"), null );
	} else if (numDauRestant == 2){
		fesElBotoClicable(null, $("#col1btn4"), null );
		fesElBotoClicable(null, $("#col2btn4"), null );
		fesElBotoClicable(null, $("#col3btn4"), null );
		fesElBotoClicable(null, $("#col4btn4"), null );
		fesElBotoClicable(null, $("#col5btn4"), null );
		fesElBotoClicable(null, $("#col6btn4"), null );
	} else if (numDauRestant == 3){
		fesElBotoClicable(null, $("#col1btn3"), null );
		fesElBotoClicable(null, $("#col2btn3"), null );
		fesElBotoClicable(null, $("#col3btn3"), null );
		fesElBotoClicable(null, $("#col4btn3"), null );
		fesElBotoClicable(null, $("#col5btn3"), null );
		fesElBotoClicable(null, $("#col6btn3"), null );
	} else if (numDauRestant == 4){
		fesElBotoClicable(null, $("#col1btn2"), null );
		fesElBotoClicable(null, $("#col2btn2"), null );
		fesElBotoClicable(null, $("#col3btn2"), null );
		fesElBotoClicable(null, $("#col4btn2"), null );
		fesElBotoClicable(null, $("#col5btn2"), null );
		fesElBotoClicable(null, $("#col6btn2"), null );
	} else if (numDauRestant == 5){
		fesElBotoClicable(null, $("#col1btn1"), null );
		fesElBotoClicable(null, $("#col2btn1"), null );
		fesElBotoClicable(null, $("#col3btn1"), null );
		fesElBotoClicable(null, $("#col4btn1"), null );
		fesElBotoClicable(null, $("#col5btn1"), null );
		fesElBotoClicable(null, $("#col6btn1"), null );
	} else if (numDauRestant == 6){
		fesElBotoClicable(arrayBtnsExteriors[0], null, null);
		fesElBotoClicable(arrayBtnsExteriors[7], null, null);
		fesElBotoClicable(arrayBtnsExteriors[14], null, null);
		fesElBotoClicable(arrayBtnsExteriors[21], null, null);
		fesElBotoClicable(arrayBtnsExteriors[28], null, null);
		fesElBotoClicable(arrayBtnsExteriors[35], null, null);
	}
	
	// deshabilitem el botó que formi part de la columna d'on ve la fitxa
	if (isOnColumnBtnBool && numDauRestant != 0){
		console.log("DELETE: col"+colExacteBtnColumn+"btn"+(6-numDauRestant));
		inhabilitaBotoConcret("col"+colExacteBtnColumn+"btn"+(6-numDauRestant));
	}
}

function iluminaBifurcacionsFork1(numDauRestant) {
	if (numDauRestant == 0){
		fesElBotoClicable(arrayBtnsExteriors[0], null, null);
	} else if (numDauRestant == 1){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[1], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[41], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col5btn1"), null);
	} else if (numDauRestant == 2){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[2], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[40], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col5btn2"), null );
	} else if (numDauRestant == 3){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[3], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[39], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col5btn3"), null );
	} else if (numDauRestant == 4){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[4], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[38], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col5btn4"), null );
	} else if (numDauRestant == 5){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[5], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[37], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col5btn5"), null );
	} else if (numDauRestant == 6){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[6], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[36], null, null);
		if (!isOnColumnBtnBool)
			iluminaBifurcacionsInicial(0);
	}
}

function iluminaBifurcacionsFork2(numDauRestant) {
	
	if (numDauRestant == 0){
		fesElBotoClicable(arrayBtnsExteriors[7], null, null);
	} else if (numDauRestant == 1){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[6], null, null);
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[8], null, null);
		if (!isOnColumnBtnBool)			
			fesElBotoClicable(null, $("#col6btn1"), null );
	} else if (numDauRestant == 2){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[5], null, null);
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[9], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col6btn2"), null );
	} else if (numDauRestant == 3){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[4], null, null);
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[10], null, null);
		if (!isOnColumnBtnBool)	
			fesElBotoClicable(null, $("#col6btn3", null) );
	} else if (numDauRestant == 4){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[3], null, null);
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[11], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col6btn4", null) );
	} else if (numDauRestant == 5){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[2], null, null);
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[12], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col6btn5"), null );
	} else if (numDauRestant == 6){
		if (!seccio1)
			fesElBotoClicable(arrayBtnsExteriors[1], null, null);
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[13], null, null);
		if (!isOnColumnBtnBool)
			iluminaBifurcacionsInicial(0);
	}
}

function iluminaBifurcacionsFork3(numDauRestant) {
	
	if (numDauRestant == 0){
		fesElBotoClicable(arrayBtnsExteriors[14], null, null);
	} else if (numDauRestant == 1){
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[13], null, null);
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[15], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col1btn1"), null );
	} else if (numDauRestant == 2){
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[12], null, null);
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[16], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col1btn2"), null );
	} else if (numDauRestant == 3){
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[11], null, null);
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[17], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col1btn3"), null );
	} else if (numDauRestant == 4){
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[10], null, null);
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[18], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col1btn4", null) );
	} else if (numDauRestant == 5){
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[9], null, null);
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[19], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col1btn5"), null );
	} else if (numDauRestant == 6){
		if (!seccio2)
			fesElBotoClicable(arrayBtnsExteriors[8], null, null);
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[20], null, null);
		if (!isOnColumnBtnBool)
			iluminaBifurcacionsInicial(0);
	}
}

function iluminaBifurcacionsFork4(numDauRestant) {
	
	if (numDauRestant == 0){
		fesElBotoClicable(arrayBtnsExteriors[21], null, null);
	} else if (numDauRestant == 1){
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[20], null, null);
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[22], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col2btn1"), null );
	} else if (numDauRestant == 2){
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[19], null, null);
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[23], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col2btn2"), null );
	} else if (numDauRestant == 3){
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[18], null, null);
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[24], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col2btn3"), null );
	} else if (numDauRestant == 4){
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[17], null, null);
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[25], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col2btn4"), null );
	} else if (numDauRestant == 5){
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[16], null, null);
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[26], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col2btn5"), null );
	} else if (numDauRestant == 6){
		if (!seccio3)
			fesElBotoClicable(arrayBtnsExteriors[15], null, null);
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[27], null, null);
		if (!isOnColumnBtnBool)
			iluminaBifurcacionsInicial(0);
	}
}

function iluminaBifurcacionsFork5(numDauRestant) {
	
	if (numDauRestant == 0){
		fesElBotoClicable(arrayBtnsExteriors[28], null, null);
	} else if (numDauRestant == 1){
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[27], null, null);
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[29], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col3btn1"), null );
	} else if (numDauRestant == 2){
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[26], null, null);
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[30], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col3btn2"), null );
	} else if (numDauRestant == 3){
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[25], null, null);
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[31], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col3btn3"), null );
	} else if (numDauRestant == 4){
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[24], null, null);
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[32], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col3btn4"), null );
	} else if (numDauRestant == 5){
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[23], null, null);
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[33], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col3btn5"), null );
	} else if (numDauRestant == 6){
		if (!seccio4)
			fesElBotoClicable(arrayBtnsExteriors[22], null, null);
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[34], null, null);
		if (!isOnColumnBtnBool)
			iluminaBifurcacionsInicial(0);
	}
}

function iluminaBifurcacionsFork6(numDauRestant) {
	
	if (numDauRestant == 0){
		fesElBotoClicable(arrayBtnsExteriors[35], null, null);
	} else if (numDauRestant == 1){
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[34], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[36], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col4btn1"), null );
	} else if (numDauRestant == 2){
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[33], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[37], null, null);			
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col4btn2"), null );
	} else if (numDauRestant == 3){
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[32], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[38], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col4btn3"), null );
	} else if (numDauRestant == 4){
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[31], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[39], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col4btn4"), null );
	} else if (numDauRestant == 5){
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[30], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[40], null, null);
		if (!isOnColumnBtnBool)
			fesElBotoClicable(null, $("#col4btn5"), null );
	} else if (numDauRestant == 6){
		if (!seccio5)
			fesElBotoClicable(arrayBtnsExteriors[29], null, null);
		if (!seccio6)
			fesElBotoClicable(arrayBtnsExteriors[41], null, null);
		if (!isOnColumnBtnBool)
			iluminaBifurcacionsInicial(0);
	}
}