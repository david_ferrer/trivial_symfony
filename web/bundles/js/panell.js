$(function () {
	$("form[action='/deletegame']").each(function(){
		$form = $(this);
		$form.find("i").on('click', function () {
			conf = confirm('Are you sure you want to delete this game?');
			if (conf)
				$(this).parent().submit();
			else
				return false;
		});
	});
	
	$("#showcreatergamebtn").on('click', function () {
		$(this).hide();
		$("#message").hide();
		$(".games-list").hide();
		$(".returnbtn").css("display", "block");
		$('#creategame').css('display', 'block');
	});
	
	$(".creategamebtn").on('click', function () {
		console.log("create!");
		
		numP = $("#numplayers").val();
		time = $("#time").val();
		
		hours = time.split(':')[0];
		minutes = time.split(':')[1];
		
		console.log("Num Players: " + numP);
		console.log("Time: " + time);
		console.log("H: " + hours );
		console.log("m: " + minutes );
		
		if (
			(hours <= 0 && minutes < 30) || numP < 1				
		){
			alert("Min: time 30min and 1 player!");
		} else {
			$("form[action='/creategame']").submit();
		}
	});
});