<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class GameController extends Controller
{
	
    /**
     * @Route("/panell", name="panell")
     */
    public function selectAllGamesAction()
    {
    	
    	if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
        $games = $this->getDoctrine()
            ->getRepository('AppBundle:Game')
            ->findAll();

        if (count($games)==0) {
            return $this->render('panell.html.twig', array(
                'message' => 'No games found'));
        }
        
        return $this->render('game/gamescontent.html.twig', array(
            'games' => $games));
      }
    }
    
    /**
     * @Route("/creategame", name="creategame")
     */
    public function insertNewGameAction(Request $request)
    {
        $game = new Game();
        
        $iduser = intval($this->getUser()->getId());

        $form = $this->createFormBuilder($game)
            ->add('numplayers', NumberType::class)
            ->add('timehours', NumberType::class)
            ->add('timeminutes', NumberType::class)
            ->add('id', HiddenType::class, array('data' => false, 'empty_data' => false))
				->add('iduser', HiddenType::class, array('data' => $iduser))
            ->add('pointsp1', HiddenType::class, array('data' => 'x'))
            ->add('pointsp2', HiddenType::class, array('data' => 'x'))
            ->add('pointsp3', HiddenType::class, array('data' => 'x'))
            ->add('pointsp4', HiddenType::class, array('data' => 'x'))
				->add('pointsp5', HiddenType::class, array('data' => 'x'))
				->add('pointsp6', HiddenType::class, array('data' => 'x'))
				->add('posxp1', HiddenType::class, array('data' => '220'))
				->add('posxp2', HiddenType::class, array('data' => '240'))
				->add('posxp3', HiddenType::class, array('data' => '260'))
				->add('posxp4', HiddenType::class, array('data' => '220'))
				->add('posxp5', HiddenType::class, array('data' => '240'))
				->add('posxp6', HiddenType::class, array('data' => '260'))
				->add('posyp1', HiddenType::class, array('data' => '220'))
				->add('posyp2', HiddenType::class, array('data' => '220'))
				->add('posyp3', HiddenType::class, array('data' => '220'))
				->add('posyp4', HiddenType::class, array('data' => '250'))
				->add('posyp5', HiddenType::class, array('data' => '250'))
				->add('posyp6', HiddenType::class, array('data' => '250'))
				->add('terminated', HiddenType::class, array('data' => false))
            ->add('save', SubmitType::class, array('label' => 'Create'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($game);
            $em->flush();
            return $this->render('panell.html.twig', array(
                'message' => 'Game inserted: '. $game->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Insert Product',
            'form' => $form->createView(),
        ));
	}
	
    /**
     * @Route("/deletegame", name="deletegame")
     */
    public function deletegame()
    {
    	
    	if (
    		$_POST && 
    		isset($_POST['id']) &&
    		isset($_POST['iduser'])
    	) {
    	
    	  $id = $_POST['id'];
    	  $iduser = $_POST['iduser'];
    	  
    	  
    	  
    	  $deleteQuery = $this->getDoctrine()
    	  		->getManager()
    	  		->createQueryBuilder('d')
    	  		->delete('AppBundle:Game', 'd')
    	  		->where('d.id = ' . $id, 'd.iduser = ' . $iduser)->getQuery();
    	  		
    	  $deletedRows = $deleteQuery->getResult();
    	  	
        $games = $this->getDoctrine()
            ->getRepository('AppBundle:Game')
            ->findAll();

        if (count($games)==0) {
        		if ($deletedRows == 0){
            	return $this->render('panell.html.twig', array(
            		'message' => 'An error has occurred during deleting game - No games found'));
            } else {
            	return $this->render('panell.html.twig', array(
            		'message' => 'Game deleted correctly - No games found'));
            }
        }
        
        return $this->render('game/gamescontent.html.twig', array(
            'games' => $games));
            
      } 
      
    }

}
