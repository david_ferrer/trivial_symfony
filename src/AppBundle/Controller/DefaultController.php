<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller{
	
	/**
	* @Route("/tauler", name="tauler")
	*/
	public function boardScreen(Request $request){
		// ruta de la vista a app/resources/views/tablero.html.twig
		
		/*
		echo " ID ID ID ID ID ID ID ";
		echo = $this->container->get('security.context')->getToken()->getUser()->getId();
		echo " <-<-<-<-";
		*/
		return $this->render("tauler.html.twig");
		
	}
	
}
