<?php
// src/AppBundle/Entity/Game.php
 
namespace AppBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
 
/**
 * @ORM\Entity
 * @ORM\Table(name="games")
 */
class Game
{
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id = null;
    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId(){ return $this->id; }

    
    /**
     * @ORM\Column(type="integer", name="iduser", length=11, nullable=false)
     */
    protected $iduser;
    
    /**
     * Get iduser
     * @return integer
     */
    public function getIdUser(){ return $this->iduser; }
    
    /**
     * @ORM\Column(type="integer", name="numplayers", length=11, nullable=false)
     */
    protected $numplayers;
    
    /**
     * Get numplayers
     * @return integer
     */
    public function getNumplayers(){ return $this->numplayers; }
    
    /**
     * @ORM\Column(type="integer", name="timehours", nullable=false)
     */
    protected $timehours;
    
    /**
     * Get timehours
     * @return integer
     */
    public function getTimeHours(){ return $this->timehours; }
    
    /**
     * @ORM\Column(type="integer", name="timeminutes", nullable=false)
     */
    protected $timeminutes;
    
    /**
     * Get timeminutes
     * @return integer
     */
    public function getTimeMinutes(){ return $this->timeminutes; }
   
    /**
     * @ORM\Column(type="text", name="pointsp1", length=255, nullable=true)
     */
    protected $pointsp1;
    
    /**
     * Get pointsp1
     * @return text
     */
    public function getPointsp1(){ return $this->pointsp1; }
    
    /**
     * @ORM\Column(type="text", name="pointsp2", length=255, nullable=true)
     */
    protected $pointsp2;
    
    /**
     * Get pointsp2
     * @return text
     */
    public function getPointsp2(){ return $this->pointsp2; }
    
    /**
     * @ORM\Column(type="text", name="pointsp3", length=255, nullable=true)
     */
    protected $pointsp3;
    
    /**
     * Get pointsp3
     * @return text
     */
    public function getPointsp3(){ return $this->pointsp3; }
    
    /**
     * @ORM\Column(type="text", name="pointsp4", length=255, nullable=true)
     */
    protected $pointsp4;
    
    /**
     * Get pointsp4
     * @return text
     */
    public function getPointsp4(){ return $this->pointsp4; }
    
    /**
     * @ORM\Column(type="text", name="pointsp5", length=255, nullable=true)
     */
    protected $pointsp5;
    
    /**
     * Get pointsp5
     * @return text
     */
    public function getPointsp5(){ return $this->pointsp5; }
    
    /**
     * @ORM\Column(type="text", name="pointsp6", length=255, nullable=true)
     */
    protected $pointsp6;
    
    /**
     * Get pointsp6
     * @return text
     */
    public function getPointsp6(){ return $this->pointsp6; }
    
    /**
     * @ORM\Column(type="integer", name="posxp1", length=11, nullable=true)
     */
    protected $posxp1;
    
    /**
     * @ORM\Column(type="integer", name="posxp2", length=11, nullable=true)
     */
    protected $posxp2;
    
    /**
     * @ORM\Column(type="integer", name="posxp3", length=11, nullable=true)
     */
    protected $posxp3;
    
    /**
     * @ORM\Column(type="integer", name="posxp4", length=11, nullable=true)
     */
    protected $posxp4;
    
    /**
     * @ORM\Column(type="integer", name="posxp5", length=11, nullable=true)
     */
    protected $posxp5;
    
    /**
     * @ORM\Column(type="integer", name="posxp6", length=11, nullable=true)
     */
    protected $posxp6;
    
    /**
     * @ORM\Column(type="integer", name="posyp1", length=11, nullable=true)
     */
    protected $posyp1;
    
    /**
     * @ORM\Column(type="integer", name="posyp2", length=11, nullable=true)
     */
    protected $posyp2;
    
    /**
     * @ORM\Column(type="integer", name="posyp3", length=11, nullable=true)
     */
    protected $posyp3;
    
    /**
     * @ORM\Column(type="integer", name="posyp4", length=11, nullable=true)
     */
    protected $posyp4;
    
    /**
     * @ORM\Column(type="integer", name="posyp5", length=11, nullable=true)
     */
    protected $posyp5;
    
    /**
     * @ORM\Column(type="integer", name="posyp6", length=11, nullable=true)
     */
    protected $posyp6;
    
    /**
     * @ORM\Column(type="boolean", name="terminated", nullable=false)
     */
    protected $terminated;
    
    /**
     * Get terminated
     * @return boolean
     */
    public function getTerminated(){ return $this->terminated; }

    /**
     * Set iduser
     *
     * @param integer $iduser
     *
     * @return Game
     */
    public function setIduser($iduser)
    {
        $this->iduser = $iduser;

        return $this;
    }

    /**
     * Set numplayers
     *
     * @param integer $numplayers
     *
     * @return Game
     */
    public function setNumplayers($numplayers)
    {
        $this->numplayers = $numplayers;

        return $this;
    }

    /**
     * Set pointsp1
     *
     * @param string $pointsp1
     *
     * @return Game
     */
    public function setPointsp1($pointsp1)
    {
        $this->pointsp1 = $pointsp1;

        return $this;
    }

    /**
     * Set pointsp2
     *
     * @param string $pointsp2
     *
     * @return Game
     */
    public function setPointsp2($pointsp2)
    {
        $this->pointsp2 = $pointsp2;

        return $this;
    }

    /**
     * Set pointsp3
     *
     * @param string $pointsp3
     *
     * @return Game
     */
    public function setPointsp3($pointsp3)
    {
        $this->pointsp3 = $pointsp3;

        return $this;
    }

    /**
     * Set pointsp4
     *
     * @param string $pointsp4
     *
     * @return Game
     */
    public function setPointsp4($pointsp4)
    {
        $this->pointsp4 = $pointsp4;

        return $this;
    }

    /**
     * Set pointsp5
     *
     * @param string $pointsp5
     *
     * @return Game
     */
    public function setPointsp5($pointsp5)
    {
        $this->pointsp5 = $pointsp5;

        return $this;
    }

    /**
     * Set pointsp6
     *
     * @param string $pointsp6
     *
     * @return Game
     */
    public function setPointsp6($pointsp6)
    {
        $this->pointsp6 = $pointsp6;

        return $this;
    }

    /**
     * Set posxp1
     *
     * @param integer $posxp1
     *
     * @return Game
     */
    public function setPosxp1($posxp1)
    {
        $this->posxp1 = $posxp1;

        return $this;
    }

    /**
     * Get posxp1
     *
     * @return integer
     */
    public function getPosxp1()
    {
        return $this->posxp1;
    }

    /**
     * Set posxp2
     *
     * @param integer $posxp2
     *
     * @return Game
     */
    public function setPosxp2($posxp2)
    {
        $this->posxp2 = $posxp2;

        return $this;
    }

    /**
     * Get posxp2
     *
     * @return integer
     */
    public function getPosxp2()
    {
        return $this->posxp2;
    }

    /**
     * Set posxp3
     *
     * @param integer $posxp3
     *
     * @return Game
     */
    public function setPosxp3($posxp3)
    {
        $this->posxp3 = $posxp3;

        return $this;
    }

    /**
     * Get posxp3
     *
     * @return integer
     */
    public function getPosxp3()
    {
        return $this->posxp3;
    }

    /**
     * Set posxp4
     *
     * @param integer $posxp4
     *
     * @return Game
     */
    public function setPosxp4($posxp4)
    {
        $this->posxp4 = $posxp4;

        return $this;
    }

    /**
     * Get posxp4
     *
     * @return integer
     */
    public function getPosxp4()
    {
        return $this->posxp4;
    }

    /**
     * Set posxp5
     *
     * @param integer $posxp5
     *
     * @return Game
     */
    public function setPosxp5($posxp5)
    {
        $this->posxp5 = $posxp5;

        return $this;
    }

    /**
     * Get posxp5
     *
     * @return integer
     */
    public function getPosxp5()
    {
        return $this->posxp5;
    }

    /**
     * Set posxp6
     *
     * @param integer $posxp6
     *
     * @return Game
     */
    public function setPosxp6($posxp6)
    {
        $this->posxp6 = $posxp6;

        return $this;
    }

    /**
     * Get posxp6
     *
     * @return integer
     */
    public function getPosxp6()
    {
        return $this->posxp6;
    }

    /**
     * Set posyp1
     *
     * @param integer $posyp1
     *
     * @return Game
     */
    public function setPosyp1($posyp1)
    {
        $this->posyp1 = $posyp1;

        return $this;
    }

    /**
     * Get posyp1
     *
     * @return integer
     */
    public function getPosyp1()
    {
        return $this->posyp1;
    }

    /**
     * Set posyp2
     *
     * @param integer $posyp2
     *
     * @return Game
     */
    public function setPosyp2($posyp2)
    {
        $this->posyp2 = $posyp2;

        return $this;
    }

    /**
     * Get posyp2
     *
     * @return integer
     */
    public function getPosyp2()
    {
        return $this->posyp2;
    }

    /**
     * Set posyp3
     *
     * @param integer $posyp3
     *
     * @return Game
     */
    public function setPosyp3($posyp3)
    {
        $this->posyp3 = $posyp3;

        return $this;
    }

    /**
     * Get posyp3
     *
     * @return integer
     */
    public function getPosyp3()
    {
        return $this->posyp3;
    }

    /**
     * Set posyp4
     *
     * @param integer $posyp4
     *
     * @return Game
     */
    public function setPosyp4($posyp4)
    {
        $this->posyp4 = $posyp4;

        return $this;
    }

    /**
     * Get posyp4
     *
     * @return integer
     */
    public function getPosyp4()
    {
        return $this->posyp4;
    }

    /**
     * Set posyp5
     *
     * @param integer $posyp5
     *
     * @return Game
     */
    public function setPosyp5($posyp5)
    {
        $this->posyp5 = $posyp5;

        return $this;
    }

    /**
     * Get posyp5
     *
     * @return integer
     */
    public function getPosyp5()
    {
        return $this->posyp5;
    }

    /**
     * Set posyp6
     *
     * @param integer $posyp6
     *
     * @return Game
     */
    public function setPosyp6($posyp6)
    {
        $this->posyp6 = $posyp6;

        return $this;
    }

    /**
     * Get posyp6
     *
     * @return integer
     */
    public function getPosyp6()
    {
        return $this->posyp6;
    }

    /**
     * Set terminated
     *
     * @param boolean $terminated
     *
     * @return Game
     */
    public function setTerminated($terminated)
    {
        $this->terminated = $terminated;

        return $this;
    }
    
  	 /**
     * Constructor
     */
     public function __construct(){}

    /**
     * Set timehours
     *
     * @param integer $timehours
     *
     * @return Game
     */
    public function setTimehours($timehours)
    {
        $this->timehours = $timehours;

        return $this;
    }

    /**
     * Set timeminutes
     *
     * @param integer $timeminutes
     *
     * @return Game
     */
    public function setTimeminutes($timeminutes)
    {
        $this->timeminutes = $timeminutes;

        return $this;
    }
}
