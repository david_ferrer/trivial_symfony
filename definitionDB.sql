DROP DATABASE IF EXISTS dbtrivial;
CREATE DATABASE IF NOT EXISTS dbtrivial;
USE dbtrivial;
  
SELECT * FROM fos_user;

CREATE TABLE dbtrivial.games(
	id INT NOT NULL AUTO_INCREMENT,
	iduser INT NOT NULL,
	numplayers INT NOT NULL,
	timehours INT NOT NULL,
	timeminutes INT NOT NULL,
	pointsp1 VARCHAR(255),
	pointsp2 VARCHAR(255),
	pointsp3 VARCHAR(255),
	pointsp4 VARCHAR(255),
	pointsp5 VARCHAR(255),
	pointsp6 VARCHAR(255),
	posxp1 INT,
	posxp2 INT,
	posxp3 INT,
	posxp4 INT,
	posxp5 INT,
	posxp6 INT,
	posyp1 INT,
	posyp2 INT,
	posyp3 INT,
	posyp4 INT,
	posyp5 INT,
	posyp6 INT,
	terminated TINYINT(1) DEFAULT 0,
	PRIMARY KEY (id, iduser),
	FOREIGN KEY (iduser) REFERENCES fos_user (id)
)Engine=InnoDB;

INSERT INTO dbtrivial.games VALUES (null, 1, 1, 1, 0, 'mbtgrv', null, null, null, null, null, 238, null, null, null, null, null, 252, null, null, null, null, null, 1);

INSERT INTO dbtrivial.games VALUES (null, "1", 1, 1, 1, "x", "x", "x", "x", "x", "x", "220", "240", "260", "220", "240", "260", "220", "220", "220", "250", "250", "250", "0");
INSERT INTO dbtrivial.games VALUES (null, 1, 1, 1, 1, "x", "x", "x", "x", "x", "x", "220", "240", "260", "220", "240", "260", "220", "220", "220", "250", "250", "250", "0");
INSERT INTO dbtrivial.games VALUES (2, 1, 1, 1, 1, "x", "x", "x", "x", "x", "x", "220", "240", "260", "220", "240", "260", "220", "220", "220", "250", "250", "250", "0");